from django.urls import re_path
from .views import my_profile
from django.conf import settings
from django.conf.urls.static import static

# url for app
urlpatterns = [
    re_path(r'^$', my_profile, name='my_profile'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

from django.shortcuts import render

# Create your views here.
page_title = 'Morizkay'


def my_profile(request):
    response = {
        'page_title': page_title
    }
    return render(request, 'my_profile.html', response)
